import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  plantEl : HTMLElement | null | undefined;
  subscription: Subscription | undefined;

  ngOnInit(): void {
    this.plantEl = document.getElementById("data_plant");
  }

  cilckObservableButton = (_event : any) => {
    if (this.plantEl) {
      let doc = this.plantEl;
      doc.style.transform = `scale(0)`;
      this.subscription = this.growPlantObservable().subscribe({
        next: (value) => {
          doc.style.transform = `scale(${value / 100})`;
        },
        error: (err) => {
          console.error(err);
        },
      });
    }
  }

  clickPromiseButton = (_event : any) => {
    if (this.plantEl) {
      let doc = this.plantEl;
      doc.style.transform = `scale(0)`;
      this.growPlantPromise()
        .then((value) => {
          doc.style.transform = `scale(${value / 100})`;
        })
        .catch((err) => {
          console.error('error', err);
        });
    }
  }

  clickUnsubscribe = (_event : any) => {
    if ( this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  growPlantObservable = () => {
    return new Observable<number>((subscriber) => {
      /*
        Observable use "subscriber.next" to show response
        When stop use "subscriber.error"
      */
      let i = 0;
      const intervalIndex = setInterval(() => {
        i++;
        if (i === 1) {
          // if (this.isFailure()) {
          //   subscriber.error('ต้นไม้เน่าไปแล้วเรียบร้อย');
          //   // clearInterval(intervalIndex);
          // }
          subscriber.next(30);
        }
        if (i === 2) {
          // if (this.isFailure()) {
          //   subscriber.error('ต้นไม้เน่าไปแล้วเรียบร้อย');
          //   // clearInterval(intervalIndex);
          // }
          subscriber.next(60);
        }
        if (i === 3) {
          // if (this.isFailure()) {
          //   subscriber.error('ต้นไม้เน่าไปแล้วเรียบร้อย');
          //   // clearInterval(intervalIndex);
          // }
          subscriber.next(100);
          subscriber.complete();
          clearInterval(intervalIndex);
        }
      }, 1000);

      return {
        unsubscribe: () => {
          clearInterval(intervalIndex);
        },
      };
    });
  }

  growPlantPromise = () =>  {
    return new Promise<number>((resolve, reject) => {
      /*
        Promise use "resolve" just one time in function
        When stop use "reject"
      */
      let i = 0;
      const intervalIndex = setInterval(() => {
        i++;
        if (i === 1) {
          // resolve(30);
          // if (this.isFailure()) {
          //   reject('ต้นไม้เน่าไปแล้วเรียบร้อย');
          //   clearInterval(intervalIndex);
          // }
        }
        if (i === 2) {
          // resolve(60);
          // if (this.isFailure()) {
          //   reject('ต้นไ dม้เน่าไปแล้วเรียบร้อย');
          //   clearInterval(intervalIndex);
          // }
        }
        if (i === 3) {
          // if (this.isFailure()) {
          //   reject('ต้นไม้เน่าไปแล้วเรียบร้อย');
          //   clearInterval(intervalIndex);
          // }
          resolve(100);
          clearInterval(intervalIndex);
        }
      }, 1000);
    });
  }

  // isFailure = () => {
  //   const randomNumber = Math.random() * 10;
  //   return randomNumber <= 3;
  // }

}



